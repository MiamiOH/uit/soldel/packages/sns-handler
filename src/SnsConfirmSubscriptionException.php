<?php


namespace MiamiOH\SnsHandler;

use Throwable;

/**
 * Class SnsConfirmSubscriptionException
 * @package MiamiOH\SnsHandler
 *
 * @codeCoverageIgnore
 */
class SnsConfirmSubscriptionException extends SnsException
{
}
