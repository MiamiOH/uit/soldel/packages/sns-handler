<?php


namespace MiamiOH\SnsHandler;

/**
 * Class SnsException
 * @package MiamiOH\SnsHandler
 *
 * @codeCoverageIgnore
 */
class SnsException extends \Exception
{
}
